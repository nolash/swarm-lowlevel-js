let content = require('./content');

module.exports = {
	Content: content.Content,
	deserializeContent: content.deserializeContent,
};
