let bmt = require('../bmt/bmt');
let chunk = require('../chunk/content');
let util = require('../util/common');

module.exports = {
	Splitter: Splitter,
	ChunkSize: bmt.ChunkSize,
	SectionSize: bmt.SectionSize,
	Branches: bmt.Branches,
};


let spanSizes = new Array(9);
let spanCounts = new Array(9);
spanSizes[0] = bmt.ChunkSize;
spanCounts[0] = 1;
for (let i = 1; i < 9; i++) {
	spanSizes[i] = bmt.Branches * spanSizes[i-1];
	spanCounts[i] = bmt.Branches * spanCounts[i-1];
}


function getLevelsFromLength(l) { 
	if (l == 0) {
		return 0;
	} else if (l <= bmt.SectionSize * bmt.Branches) {
		return 1;
	}
	let c = (l - 1)	/ bmt.SectionSize;
	return Math.floor(Math.log(c)/Math.log(bmt.Branches))+1;
}


// Hasher performs Swarm's file hasher algorithm on data
// if not undefined, the callback function receives the intermediate chunks, in the following structure:
// {
//	level: Level in tree (0 is data level)
//	index: Index of chunk on tree level
//	reference: The Swarm reference of the chunk
//	data: The chunk data
// }
function Splitter(chunkCallback, returnChunk=false) {
	this.levels = 0;
	this.bmt = new bmt.Hasher();
	this.cursors = new Array(9);
	this.length = 0;
	this.buffer = Buffer.alloc(bmt.ChunkSize*2*9);
	this.cb = chunkCallback;
	this.counts = new Array(9);
	if (returnChunk) {
		this.lastChunk = new chunk.Content();
	} else {
		this.lastChunk = undefined;
	}
	if (chunkCallback !== undefined || returnChunk) {
		this.saveChunk = true;
	} else {
		this.saveChunk = false;
	}
	this.reset = function() {
		for (let i = 0; i < 9; i++) {
			this.cursors[i] = 0;
			this.counts[i] = 0;
		}
		this.length = 0;
		this.levels = 0;
	};
	this.reset();
}


Splitter.prototype.split = function(b) {
	let l = bmt.ChunkSize;
	for (let i = 0; i < b.length; i += bmt.ChunkSize) {
		if (b.length - i < bmt.ChunkSize) {
			l = b.length - i;
		}
		let buf = b.slice(i, i+l);
		this.update(0, buf);
	}
	return this.digest();
}

Splitter.prototype.update = function(lvl, data) {
	if (lvl == 0) {
		this.length += data.length;
	}
	for (let i = 0; i < data.length; i++) {
		this.buffer[this.cursors[lvl]] = data[i];
		this.cursors[lvl]++;
	}
	if (this.cursors[lvl] - this.cursors[lvl+1] == bmt.ChunkSize) {
		let ref = this.sum(lvl);

		// write the resulting reference to the level above
		this.update(lvl+1, ref);

		// recycle the space used by the hashed data
		this.cursors[lvl] = this.cursors[lvl+1];

	}
}


Splitter.prototype.sum = function(lvl) {

	// increase the hasher count on this level
	this.counts[lvl]++;

	// calculate the actual length of the data under the chunk
	let spanSize = spanSizes[lvl];
	let span = (this.length-1) % spanSize + 1;

	// generate the data to be written to the BMT hasher
	let size = this.cursors[lvl] - this.cursors[lvl+1];
	let bufPart = this.buffer.slice(this.cursors[lvl+1], this.cursors[lvl+1]+size);

	// hash the data
	this.bmt.reset();
	this.bmt.setSpan(span);
	this.bmt.update(bufPart);
	let ref = this.bmt.digest();

	if (this.saveChunk) {
		spanCopy = Uint8Array.from(this.bmt.span);
		dataCopy = Uint8Array.from(bufPart);
		refCopy = Uint8Array.from(ref);

		let ch = new chunk.Content(refCopy, dataCopy, spanCopy);
		ch.setMeta(lvl, this.counts[lvl]-1);

		// if callback is registered, output the data to it
		// TODO: spurious call with size 0
		if (typeof this.cb !== 'undefined' && size > 0) {
			this.cb(ch);
		}

		if (typeof this.lastChunk !== 'undefined') {
			this.lastChunk = ch;
		}

	}

	return ref;
}


Splitter.prototype.digest = function() {

	// if we did not end on a chunk boundary, the last chunk hasn't been hashed
	// we need to do this first
	if (this.length % bmt.ChunkSize != 0) {
		let ref = this.sum(0);
		for (let i = 0; i < bmt.SectionSize; i++) {
			this.buffer[this.cursors[1]] = ref[i];
			this.cursors[1]++;
		}
		this.cursors[0] = this.cursors[1];
	}

	// get number of tree levels the data size will produce
	this.levels = getLevelsFromLength(this.length);

	// iterate levels and sum remaining chunks until root
	for (let i = 1; i < this.levels; i++) {

		// if there is a single reference outside a balanced tree on this level
		// don't hash it again but pass it on to the next level
		// if (this.counts[i-1] - this.counts[i] == bmt.Branches) {
		if (this.counts[i] > 0) {
			if (this.counts[i-1] - spanCounts[this.levels-i-1] <= 1) {
				this.cursors[i+1] = this.cursors[i];
				this.cursors[i] = this.cursors[i-1];
				continue;
			}
		}
	
		// sum and write to level above
		let ref = this.sum(i);
		for (let j = 0; j < ref.length; j++) {
			this.buffer[this.cursors[i+1]] = ref[j];
			this.cursors[i+1]++;
		}
		this.cursors[i] = this.cursors[i+1];
	}

	if (this.lastChunk != undefined) {
		return this.lastChunk;
	} 
	return Uint8Array.from(this.buffer.slice(0, bmt.SectionSize));
}
