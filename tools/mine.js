#!/usr/bin/env node

var bzz = require('../unsafewallet');

let prefix = process.argv[2];
let match = false;

while (!match) {
	let w = new bzz.Wallet();
	console.log("trying " + w.getBzzKey());
	if (w.getBzzKey().substr(0, prefix.length) == prefix) {
		match = true
		console.log(w.getPrivateKey());
		console.log(w.getAddress());
		console.log(w.getBzzKey());
	}
}
