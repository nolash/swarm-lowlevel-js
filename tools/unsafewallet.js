#!/usr/bin/env node

var bzz = require('../unsafewallet/unsafewallet.js');
var fs = require('fs');

let keyhx = undefined;
if (process.argv.length > 2 && process.argv[2].length == 64) {
	keyhx = process.argv[2]; 
}
let w = new bzz.Wallet(keyhx);

let filename = w.getAddress();

let fd = fs.openSync(filename + ".priv.bin", "w", 0600);
let data = w.getPrivateKey("bytes");
let c = fs.writeSync(fd, data);
if (c != data.length) {
	cleanup(filename);
}

fd = fs.openSync(filename + ".pub.bin", "w", 0600);
data = w.getPublicKey("bytes");
c = fs.writeSync(fd, data);
if (c != data.length) {
	cleanup(filename);
}

fd = fs.openSync(filename + ".bzzkey.bin", "w", 0600);
data = w.getBzzKey("bytes");
c = fs.writeSync(fd, data);
if (c != data.length) {
	cleanup(filename);
}
console.log(w.getAddress());
console.log(w.getBzzKey());

fd = fs.openSync(filename + ".bin", "w", 0600);
data = w.getAddress("bytes");
c = fs.writeSync(fd, data);
if (c != data.length) {
	cleanup(filename);
}

function cleanup(filename) {
	fs.unlinkSync(filename + ".priv.bin");
	fs.unlinkSync(filename + ".pub.bin");
	fs.unlinkSync(filename + ".bin");
	process.exit(1);
}
