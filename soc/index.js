let soc = require('./soc');

module.exports = {
	Soc: soc.Soc,
	newFromData: soc.newFromData,
	newFromSocChunk: soc.newFromSocChunk,
	digestToSign: soc.digestToSign,
	idLength: soc.idLength,
	signatureLength: soc.signatureLength,
	deserialize: soc.deserialize,
	getChunk: soc.getChunk,
};
