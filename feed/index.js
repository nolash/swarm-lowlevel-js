let feed = require('./feed');

module.exports = {
	Request: feed.Request,
	Epoch: feed.Epoch,
	topicLength: 32,
};
