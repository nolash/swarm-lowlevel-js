# swarm-lowlevel

Utilities for Swarm the decentralized cloud storage system. Please refer to the SEE ALSO section to read more about the concepts involved in this module.

Currently the module offers tooling for:
* BMT hasher
* File splitter (previously file hasher)
* File joiner
* SOC

In absence of friendly instructions please refer to the `example` directory and tests for hints on what this is and how to use it. I've run it successfully with [Swarm v0.4.3](https://github.com/ethersphere/swarm/tree/v0.4.3) 

There is also an executable in `tools` that generates a random private key and writes it to a binary file along with its public key and ethereum address. The _resulting files_ are named after the latter.

## INSTALL

`npm install swarm-lowlevel`

The script `check.sh` checks file checksums and signatures.

My GPG key is [0826EDA1702D1E87C6E2875121D2E7BB88C2A746](http://keys.gnupg.net/pks/lookup?op=vindex&fingerprint=on&search=0x21D2E7BB88C2A746). Potential key authenticators:

* https://holbrook.no/louis_dev_pub.asc
* https://gitlab.com/nolash (see profile)
* https://github.com/nolash (see profile)

## USAGE

### BMT HASHER

```
let swarm = require('swarm-lowlevel')

let bh = new swarm.bmtHasher()

let data = Buffer.from('foo');
bh.reset(data.length);
bh.setSpan(data.length);
bh.update(data);
let bmthash = bh.digest(data);

console.debug('bmt hash', bmthash)

---

> bmt hash <Buffer 23 87 e8 e7 d8 a4 8c 2a 93 39 c9 7c 1d c3 46 1a 9a 7a a0 7e 99 4c 5c b8 b3 8f d7 c1 b3 e6 ea 48>
```

### FILE HASHER

```
let swarm = require('swarm-lowlevel')

let fh = new swarm.fileSplitter(console.debug);

let data = Buffer.from('foo');
let swarmhash = fh.split(data)

console.debug('file hash', swarmhash)

---

> {
>   level: 0,
>   index: 0,
>   reference: <Buffer 23 87 e8 e7 d8 a4 8c 2a 93 39 c9 7c 1d c3 46 1a 9a 7a a0 7e 99 4c 5c b8 b3 8f d7 c1 b3 e6 ea 48>,
>   data: <Buffer 66 6f 6f>
> }
>
> file hash <Buffer 23 87 e8 e7 d8 a4 8c 2a 93 39 c9 7c 1d c3 46 1a 9a 7a a0 7e 99 4c 5c b8 b3 8f d7 c1 b3 e6 ea 48>
```

### FILE JOINER

```
let swarm = require('swarm-lowlevel')

yourCustomChunkGetter.prototype.get = function(chunk) {
	console.debug('chunk', chunk);
}

let getter = new yourCustomChunkGetter(...);
let j = new joiner.Joiner(getter, process.stdout.write);

let hashOfFoo = '2387e8e7d8a48c2a9339c97c1dc3461a9a7aa07e994c5cb8b38fd7c1b3e6ea48';
j.join(hashOfFoo);
```

### SOC

```
let swarm = require('swarm-lowlevel');

let hashOfFoo = '2387e8e7d8a48c2a9339c97c1dc3461a9a7aa07e994c5cb8b38fd7c1b3e6ea48';
let signer = new swarm.unsafeWallet();
let soc_ch = undefined;
let cb = function(ch) {
	soc_ch = ch;
	console.debug(ch);
}
let soc = swarm.socFromData(hashOfFoo, Buffer.from('there can be only one'), signer, cb);
soc.sign();

let soc_recovered = swarm.socFromSocChunk(soc_ch);
let chunk = soc_recovered.getChunk();
console.debug(String.fromCharCode.apply(null, chunk.data));

soc = new swarm.soc(hashOfFoo, chunk, undefined);
chunk = soc.getChunk();
console.debug(String.fromCharCode.apply(null, chunk.data));

> {
>   level: 0,
>   index: 0,
>   span: undefined,
>   reference: <Buffer 69 f1 94 be 66 bd 30 bb 64 6b 20 34 e0 bd 0b 16 3d 1f 85 e7 a9 24 a4 4d 65 ec 7d bf 23 7f 91 45>,
>   data: Uint8Array(126) [
>      35, 135, 232, 231, 216, 164, 140,  42, 147,  57, 201, 124,
>      29, 195,  70,  26, 154, 122, 160, 126, 153,  76,  92, 184,
>     179, 143, 215, 193, 179, 230, 234,  72,  31,  79,  57, 178,
>     202, 206, 205, 187,   0, 190, 170, 130,  95, 109,   8, 168,
>     134, 144,  24,  18, 224, 146, 198, 107, 238,   3,  79, 162,
>     179, 148, 177,  64,  11,  18, 254,  25,  18, 126, 216, 176,
>     116, 126,  60,  77,  34, 127,  47, 206, 155, 183,  60, 137,
>      43, 109,  68, 176, 129,  85, 254, 126,  77,  33,  83,  14,
>      30,  21,   0,   0,
>     ... 26 more items
>   ]
> }
> from soc: there can be only one
> from chunk: there can be only one
```

## SEE ALSO

* https://swarm.ethereum.org
* https://swarm-guide.readthedocs.io
* https://github.com/ethersphere/swarm

## LICENSE

GPLv3
