let bmt = require('./bmt');

module.exports = {
	digestSize: bmt.digestLength,
	sectionSize: bmt.SectionSize,
	branches: bmt.Branches,
	chunkSize: bmt.ChunkSize,
	spanSize: bmt.spanSize,
	Hasher: bmt.Hasher,
};
