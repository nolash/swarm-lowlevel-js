let bigInt = require('big-integer');

module.exports = {
    writeBigUInt64LE,
}

function verifyBigUInt64(n) {
    if (typeof(n) === 'number') {
        if (n < 0) {
            throw "not a valid positive integer";
        }
        if (!Number.isSafeInteger(n)) {
            throw "not a safe integer";
        }
		return n;
    } else if (typeof(n) === 'bigint') {
        if (n < 0) {
            throw "not a valid positive integer";
        }
        if (n >= BigInt(2) ** BigInt(64)) {
            throw "not a valid value";
        }
        return n;
    } else {
        throw "not a valid bigint or convertible value";
    }
}

function writeBigUInt64LE(n) {
    verifyBigUInt64(n);
    let poly = bigInt(n);
    let b = Buffer.from(poly.toArray(256).value.reverse());
    let diff = 8 - b.length;
    let pad = Buffer.alloc(diff);
    return Buffer.concat([b, pad]);
}

