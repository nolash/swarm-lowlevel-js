let bigInteger = require('big-integer');
let assert = require('assert');
let bigint = require('../util/bigint');

describe('bigInt polyfill', () => {
    it('works with BigInt', () => {
        let bi = BigInt(2n ** 256n - 1n);
        let polyfill = bigInteger(bi);
        let s = polyfill.toString(16);

        assert.strictEqual(s, 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff');
    })

    it('works with hex string', () => {
        let hexString = 'ffeeddccbbaa99887766554433221100';
        let bi = bigInteger(hexString, 16)
        let s = bi.toString(16);
        assert.strictEqual(s, hexString);
    })
})

describe('writeBigUInt64LE polyfill', () => {
    it('produces same value for biggest possible value', () => {
        let span = new Buffer.allocUnsafe(8);
        let bi = 2n ** 64n - 1n;
        span.writeBigUInt64LE(bi);

        let span2 = bigint.writeBigUInt64LE(bi)

        assert.deepStrictEqual(span, span2);
    })

    it('produces same value for 32 bit', () => {
        let span = new Buffer.allocUnsafe(8);
        let n = 0xfffffff7;
        span.writeBigUInt64LE(BigInt(n));

        let span2 = bigint.writeBigUInt64LE(n);

        assert.deepStrictEqual(span, span2);
    })

    it('produces same value for 8 bit', () => {
        let span = new Buffer.allocUnsafe(8);
        let n = 0xf7;
        span.writeBigUInt64LE(BigInt(n));

        let span2 = bigint.writeBigUInt64LE(n);

        assert.deepStrictEqual(span, span2);
    })

    it('produces same value for zero', () => {
        let span = new Buffer.allocUnsafe(8);
        let n = 0;
        span.writeBigUInt64LE(BigInt(n));

        let span2 = bigint.writeBigUInt64LE(n);

        assert.deepStrictEqual(span, span2);
    })

    it('throws error for too big value', () => {
        let bi = 2n ** 64n;

        assert.throws(() => bigint.writeBigUInt64LE(bi))
    })

    it('throws error for negative value', () => {
        let n = -1;

        assert.throws(() => bigint.writeBigUInt64LE(n))
    })

    it('throws error for non-integer value', () => {
        let n = Math.PI;

        assert.throws(() => bigint.writeBigUInt64LE(n))
    })
})

