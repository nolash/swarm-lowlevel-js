let fs = require('fs');
let bmt = require('../bmt/bmt');
let joiner = require('../file/join');

let CHUNKSIZE = 4096;

function getFromDisk(hash) {
	let buf = new ArrayBuffer(bmt.ChunkSize+8)
	let fd = fs.openSync('./chunks/' + hash, 'r');
	let array = new Uint8Array(buf);
	let c = fs.readSync(fd, array);
	fs.closeSync(fd);
	return buf.slice(0, c);
}

function outCallback(data) {
	process.stdout.write(data.data);
}

//console.debug(getFromDisk('29a5fb121ce96194ba8b7b823a1f9c6af87e1791f824940a53b5a7efe3f790d9'));
let j = new joiner.Joiner(getFromDisk, outCallback);
//console.debug(getFromDisk('
//j.join('29a5fb121ce96194ba8b7b823a1f9c6af87e1791f824940a53b5a7efe3f790d9');
//j.join('61416726988f77b874435bdd89a419edc3861111884fd60e8adf54e2f299efd6');
j.join('485a526fc74c8a344c43a4545a5987d17af9ab401c0ef1ef63aefcc5c2c086df');
